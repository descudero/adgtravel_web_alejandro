export function custom(flikity){
    //Flickity Use Example
    $( document ).ready(function() {
        //La clase .main-carousel es la clase que contiene la lista de elementos
        var galleryElems = document.querySelectorAll('.main-carousel');
        for ( var i=0, len = galleryElems.length; i < len; i++ ) {
            var galleryElem = galleryElems[i];
            var flk = new flikity( galleryElem, {
                cellAlign: 'center',
                contain: true,
                groupCells: true,
                pageDots: false,
                lazyLoad: 1,
                fullscreen:true,
            });
            flk.on( 'fullscreenChange', function( isFullscreen ) {
                if(isFullscreen){
                    $('.flickity-button.flickity-fullscreen-button .text').text(textos.cerrarFullScreen);
                }else{
                    $('.flickity-button.flickity-fullscreen-button .text').text(textos.ampliarImagen);
                }
            });
        }
        $('.flickity-button.flickity-fullscreen-button').append('<p class="text">'+textos.ampliarImagen+'</p>');
    });
}
